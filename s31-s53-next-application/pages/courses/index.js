import { useContext } from 'react';
import CourseCard from '../../components/CourseCard';
import { Table, Button, Container } from 'react-bootstrap';
import coursesData from '../../data/courses';
import UserContext from '../../UserContext';
import Head from 'next/head';//lets acquire the needed components and packages in order to build our courses page. 

export async function getServerSideProps(){
  const res = await fetch('http://localhost:4000/api/courses')
  const coursesData = await res.json()
  return { props: { coursesData }}
}

//lets create a function that return our courses catalog/page. 
export default function index({coursesData}){
	//use the UserContext and we destructured it to access the user that we will define in the App component. 
	const { user } = useContext(UserContext)
  console.log(coursesData)
  const courses = coursesData.map(courseData => {
    return (
        <CourseCard key={courseData._id} courseProp={courseData}/>
    )
  })  //.map to get inividual course(courseData) from coursesData, once you have catch the courses individualy we are now going to place them inside courseCard component

		
	//lets create rows to be rendered in a bootstrap table when an admin is logged in, in our courses catalog this will also have an admin panel section 
	const courseRows = coursesData.map(courseData => {
    return(
      <tr key={courseData._id}>
        <td>{courseData._id}</td>
        <td>{courseData.name}</td>
        <td>{courseData.price}</td>
        <td>{courseData.onOffer ? 'open' : 'closed'}</td>
        <td>{courseData.start_date}</td>
        <td>{courseData.end_data}</td>
        <td>
          <Button className="bg-warning">Update</Button>
          <Button className="bg-danger">Disable</Button>
        </td>
      </tr>
    )
  })
    //lets create a ternary that will return the proper display depending on the user type. 
		return (
            user.isAdmin === true 
            ? 
            <Container>
	            <Head>
	               <title>Courses Admin Dashboard</title>
	            </Head>
	            <h1>Course Dashboard</h1>
                <Table striped bordered hover>
                    <thead>
                       <tr>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Price</th>
                          <th>Status</th>
                          <th>Start Date</th>
                          <th>End Date</th>
                          <th>Actions</th>                       
                       </tr>
                    </thead> 
                    <tbody>
                    	{ courseRows }
                    </tbody>               	
                </Table>	            
	              </Container> 
                  :
                <Container>
                <Head>
                  <title>Courses Index</title>
                </Head>
                   { courses } 
                </Container>		
		)
}