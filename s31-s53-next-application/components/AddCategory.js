import { useState, useEffect, useContext } from 'react'
import { Form, Button, Col, Card, Container } from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'
import styles from './AddCategory.module.css'

export default function AddCategory({setCategories}) {
    const {user} = useContext(UserContext)
    const [categoryName, setCategoryName] = useState('')
    const [categoryType, setCategoryType] = useState('Expense')
    const [isActive, setIsActive] = useState(false)

    useEffect(()=> {
        (categoryName.length < 30 && categoryName.length > 0)
        ? setIsActive(true)
        : setIsActive(false)
    }, [categoryName])

    function addCategory(e) {
        e.preventDefault()
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${user.id}/categories`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                categoryType: categoryType
            })
        })
        .then(res => res.json())
        .then(data => {
            data
            ?   fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
                })
                .then(res => res.json())
                .then(data => {
                    setCategories(data.categories)
                    setCategoryName('')
                    Swal.fire({
                    text: 'Category has been added!',
                    icon: 'success',
                    timer: 800,
                    timerProgressBar: true,
                    showConfirmButton: false
                    })
                    .then((result) => {
                        result.dismiss === Swal.DismissReason.timer
                        ? null
                        : null
                    })
                })
            :   Swal.fire({
                text: 'This is an existing category. Please try again!', icon: 'error',
                timer: 800,
                timerProgressBar: true,
                showConfirmButton: false
                })
                .then((result) => {
                    result.dismiss === Swal.DismissReason.timer
                    ? null
                    : null
                })
        })
    }

    return (
        <Form onSubmit={(e) => addCategory(e)} className={styles.form}>
        <Container className="mt-5 pl-0 pr-0 my-2">
        <Card>
        <Card.Body className="width-size">
            <Form.Row>
            <div className="col-md-12">
                <Form.Label className={styles.formItems} column>Add Category:</Form.Label>
                    <Col className={styles.formItems}><br/>
                        <Form.Control className={styles.formInput} type="text" placeholder="Category Name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required />
                    </Col>
            </div>
            </Form.Row>
            <Form.Row>
                <div className="col-md-12">
                    <Form.Label  className={styles.formItems} column>Select Type:</Form.Label>
                        <Col className={styles.formItems}><br/>
                            <Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)} required >
                                <option>Expense</option>
                                <option>Income</option>
                            </Form.Control>
                        </Col>
                </div>
            </Form.Row>
            <Form.Row>
                <Col xs md="9">
                </Col>
                <Col xs md="3">
                    { isActive === true
                    ? <Button className={styles.button} type="submit" size="lg" variant="success" block><h6>ADD</h6></Button>
                    : <Button className={styles.button}type="submit" variant="outline-success" block disabled><h6>ADD</h6></Button>}
                </Col>
            </Form.Row>
        </Card.Body>
        </Card>
        </Container>
        </Form>
    )
}